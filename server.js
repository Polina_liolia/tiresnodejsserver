//including module for auto-configuration:
var config = require('cloud-env');

// including connect module:
var connect = require('connect');

var fs = require('fs');

// to process the requests:
function send_trailer_tires_catalog(req, res)
{
    var obj = JSON.parse(fs.readFileSync('trailer_tires_catalog.json', 'utf8'));
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(obj));
}

function send_truck_tires_catalog(req, res)
{
    var obj = JSON.parse(fs.readFileSync('truck_tires_catalog.json', 'utf8'));
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(obj));
}

function send_truckforces(req, res)
{
    var obj = JSON.parse(fs.readFileSync('truckforces.json', 'utf8'));
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(obj));
}

function send_speed_indexes(req, res) {
    var obj = JSON.parse(fs.readFileSync('speed_indexes.json', 'utf8'));
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(obj));
}

function send_load_indexes(req, res) {
    var obj = JSON.parse(fs.readFileSync('load_indexes.json', 'utf8'));
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(obj));
}

function send_pressure_units(req, res) {
    var obj = JSON.parse(fs.readFileSync('pressure_units.json', 'utf8'));
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(obj));
}

var app = connect();
app.use(connect.compress());
app.use(connect.query());
app.use('/trailer_tires_catalog', send_trailer_tires_catalog);
app.use('/truck_tires_catalog', send_truck_tires_catalog);
app.use('/truckforces', send_truckforces);
app.use('/speed_indexes', send_speed_indexes);
app.use('/load_indexes', send_load_indexes);
app.use('/pressure_units', send_pressure_units);

app.listen(config.PORT, config.IP, function () {
    console.log("Listening on "+config.IP+", port "+config.PORT)
});